import nltk
from nltk import FreqDist
import os

def preprocess_unicode(string_input):
    return string_input.replace("\\xc3\\xb6", "ö")\
                       .replace("\\xc3\\xa4", "ä")\
                       .replace("\\xc3\\xbc", "ü")\
                       .replace("\\xe2\\x80\\x99", "'")\
                       .replace("\\xe2\\x80\\x93", "–")\
                       .replace("\\xc3\\x9f", "ß")\
                       .replace("\\n", " ")\
                       .replace("\\xe2\\x80\\x9e", "„")\
                       .replace("\\xe2\\x80\\xa6", "…")\
                       .replace("\\xf0\\x9f\\xa4\\xa3", "🤣")\
                       .replace("\\xf0\\x9f\\x98\\x82", "😂")\
                       .replace("\\xc2\\xb4", "´")\
                       .replace("\\xc3\\x84", "Ä")\
                       .replace("\\xe2\\x80\\x98", "‘")\
                       .replace("\\xe2\\x82\\xac", "€")\
                       .replace("\\xe2\\x9d\\x84\\xef\\xb8\\x8f", "")\
                       .replace("\\xc3\\x9c", "Ü")\
                       .replace("\\xe2\\x80\\x9c", "“")

stopwords = ['#', ':', 'https', '@', ',', '.', "b'RT", 'b', 'in', '(', ')' '!', "'", 'die', 'der', 'DB', "''",\
             'db', 'und', 'mit', 'for', 'ist', 'Bahn', 'für', 'dienasbizness', '?', 'fredykoglin', '...', 'Ein',\
             'Eisenbahn', 'im', 'via', 'ein', '``', 'den', 'es', '-', "dblv'", 'von', 'DBRegio', 'auf', 'Tag',\
             'Lokführer', 'so', 'SBahnBerlin', 'A', 'vier', ')', '!', 'DBGT', 'Dragon', 'DragonBallZ', 'dragonball' ]


words_of_interest = ["fehlt", "klappt", "Störung", "zufrieden", "\\xF0\\x9F\\x98\\x9E"]

filenames = os.listdir(os.getcwd() + "/data")
for f in filenames: print(f)

tweets = ""

for filename in filenames:
    tweets += open(os.getcwd() + "/data/" + filename).read()

#Preprocessing
tweets = preprocess_unicode(tweets)
print(tweets)

#Tokenization
tokens = nltk.word_tokenize(tweets)
print(tokens)


#Spliting Data into single tweet list
tweet_sents = tweets.split("b'")

t1 = nltk.text.Text(tokens)
fdist1 = FreqDist(t1)
common_tokens = [(tokens, x) for (tokens, x) in fdist1.most_common(200) if tokens not in stopwords]
for e in common_tokens:
    print(e)
t1.concordance("fehlt")
t1.concordance("Störung")

#for sent in tweet_sents: print(sent)

for tweet in tweet_sents:
    if any(word in tweet for word in words_of_interest):
        print(tweet)
